import request from 'supertest';
import { app } from '../src';
import { startTime } from '../src/utils';
import moment from 'moment';

describe('/', () => {
  it('returns status 200', async () => {
    const response = await request(app).get('/');

    expect(response.status).toEqual(200);
  });

  it('startTime is actually a date', async () => {
    const startTimeDate = moment(startTime).toDate();

    expect(startTimeDate instanceof Date).toEqual(true);
    expect(startTimeDate.toString()).not.toEqual('Invalid Date');
  });
});
