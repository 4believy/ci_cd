import moment from 'moment';

export const startTime = moment().utc().format('YYYY/MM/DD HH:mm:ss');
