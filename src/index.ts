import express, { Express, Request, Response } from 'express';
import dotenv from 'dotenv';
import { startTime } from './utils';

dotenv.config();

export const app: Express = express();

export const port = process.env.PORT || 8001;
app.get('/', (req: Request, res: Response) => {
  const [day, time]: string[] = startTime.split(' ');

  res.send(
    `Express + Typescript Server. App was deployed on ${day} at ${time} UTC`,
  );
});

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
